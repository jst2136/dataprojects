# README #

To view .ipynb files, please render to viewable ipython notebook by viewing raw JSON in bitbucket, copying the corresponding url, then pasting url in NBViewer.jupyter.org:

http://nbviewer.jupyter.org/

If you encounter "Hmm... can't find that one" bitbucket error message, unselect "New source browser experience BETA", which can be found at: account (Bottom left) > BitBucket Labs > New source browser experience BETA.

Thanks.


